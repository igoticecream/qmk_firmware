/*
Config to suspend RGB when computer is off or sleeping
See: https://beta.docs.qmk.fm/using-qmk/hardware-features/lighting/feature_rgb_matrix#suspended-state-id-suspended-state
*/
#define RGB_DISABLE_WHEN_USB_SUSPENDED true
